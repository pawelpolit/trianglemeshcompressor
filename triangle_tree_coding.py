# __author__ = 'Pawel Polit'

from Queue import PriorityQueue

from triangle_tree import TriangleTreeNode


class HuffmanTreeNode(object):
    def __init__(self, left_child, right_child):
        self.left_child = left_child
        self.right_child = right_child


def preorder_tree_encoding(root, encoded_tree, encoded_triangles):
    encoded_triangles.append(root.left_vertex())
    encoded_triangles.append(root.right_vertex())

    stack = [root]

    while len(stack) > 0:
        current_node = stack.pop()
        encoded_triangles.append(current_node.center_vertex())

        while current_node.has_left_child():
            if current_node.has_right_child():
                encoded_tree.append((1, 1))
                stack.append(current_node.right_child())
            else:
                encoded_tree.append((1, 0))

            current_node = current_node.left_child()
            encoded_triangles.append(current_node.center_vertex())

        if current_node.has_right_child():
            encoded_tree.append((0, 1))
            stack.append(current_node.right_child())
        else:
            encoded_tree.append((0, 0))


def preorder_tree_decoding(encoded_tree, encoded_triangles):
    next_index = 0

    root = TriangleTreeNode(next_index)
    next_index += 1
    root.set_left_vertex(encoded_triangles.pop(0))
    root.set_right_vertex(encoded_triangles.pop(0))

    stack = []

    current_node = root

    for iterator in range(len(encoded_tree)):
        current_node.set_center_vertex(encoded_triangles[iterator])

        if encoded_tree[iterator][1] == 1:
            current_node.set_right_child(TriangleTreeNode(next_index, current_node))
            next_index += 1
            current_node.right_child().set_left_vertex(current_node.center_vertex())
            current_node.right_child().set_right_vertex(current_node.right_vertex())

            stack.append(current_node.right_child())

        if encoded_tree[iterator][0] == 1:
            current_node.set_left_child(TriangleTreeNode(next_index, current_node))
            next_index += 1
            current_node.left_child().set_left_vertex(current_node.left_vertex())
            current_node.left_child().set_right_vertex(current_node.center_vertex())

            current_node = current_node.left_child()
        elif len(stack) > 0:
            current_node = stack.pop()

    return root


def prepare_huffman_tree(encoded_tree):
    number_of_codes = {
        (0, 0): 0,
        (0, 1): 0,
        (1, 0): 0,
        (1, 1): 0
    }

    for node_code in encoded_tree:
        number_of_codes[node_code] += 1

    queue = PriorityQueue()

    for code in number_of_codes:
        queue.put((number_of_codes[code], code))

    for _ in range(3):
        left_child_frequency, left_child = queue.get()
        right_child_frequency, right_child = queue.get()
        queue.put((left_child_frequency + right_child_frequency, HuffmanTreeNode(left_child, right_child)))

    return queue.get()[1]


def huffman_tree_preorder(node, current_code, encoding, encoded_huffman_tree):
    if type(node) == tuple:
        encoded_huffman_tree.append(0)
        encoded_huffman_tree.append(node[0])
        encoded_huffman_tree.append(node[1])
        encoding[node] = current_code
    else:
        encoded_huffman_tree.append(1)
        huffman_tree_preorder(node.left_child, current_code + [0], encoding, encoded_huffman_tree)
        huffman_tree_preorder(node.right_child, current_code + [1], encoding, encoded_huffman_tree)


def find_encoding_and_encode_huffman_tree(huffman_tree_root):
    encoding = {}
    encoded_huffman_tree = []

    huffman_tree_preorder(huffman_tree_root, [], encoding, encoded_huffman_tree)

    for key in encoding:
        if len(encoding[key]) == 1:
            encoding[key] = encoding[key][0]
        else:
            encoding[key] = tuple(encoding[key])

    return encoding, encoded_huffman_tree


def huffman_encoding(encoded_tree):
    huffman_tree_root = prepare_huffman_tree(encoded_tree)
    encoding, encoded_huffman_tree = find_encoding_and_encode_huffman_tree(huffman_tree_root)
    return map(lambda code: encoding[code], encoded_tree), encoded_huffman_tree


def huffman_decoding(encoded_tree, encoded_huffman_tree):
    encoded_huffman_tree_copy = encoded_huffman_tree[:]
    huffman_tree_root = retrieve_huffman_tree(encoded_huffman_tree_copy)
    encoding = find_encoding_and_encode_huffman_tree(huffman_tree_root)[0]
    inverse_encoding = {value: key for key, value in encoding.items()}
    return map(lambda code: inverse_encoding[code], encoded_tree)


def retrieve_huffman_tree(encoded_huffman_tree):
    if encoded_huffman_tree.pop(0) == 0:
        return encoded_huffman_tree.pop(0), encoded_huffman_tree.pop(0)
    else:
        return HuffmanTreeNode(retrieve_huffman_tree(encoded_huffman_tree), retrieve_huffman_tree(encoded_huffman_tree))


def possible_codes(encoded_huffman_tree):
    encoded_huffman_tree_copy = encoded_huffman_tree[:]
    return find_encoding_and_encode_huffman_tree(retrieve_huffman_tree(encoded_huffman_tree_copy))[0].values()


def encode(root):
    encoded_tree = []
    encoded_triangles = []
    preorder_tree_encoding(root, encoded_tree, encoded_triangles)
    encoded_tree, encoded_huffman_tree = huffman_encoding(encoded_tree)
    return encoded_triangles, encoded_tree, encoded_huffman_tree


def decode(encoded_triangles, encoded_tree, encoded_huffman_tree):
    encoded_tree = huffman_decoding(encoded_tree, encoded_huffman_tree)
    return preorder_tree_decoding(encoded_tree, encoded_triangles)
