# __author__ = 'Pawel Polit'

import struct

import triangle_tree_coding


class EncodedTreeRepresentationBuilder(object):
    def __init__(self):
        self.__encoded_tree_representation__ = []
        self.__current_element__ = ''

    def add_bits(self, bits):
        if type(bits) == tuple:
            for bit in bits:
                self.__add_bit__(bit)
        else:
            self.__add_bit__(bits)

    def __add_bit__(self, bit):
        self.__current_element__ += str(bit)

        if len(self.__current_element__) == 8:
            self.__encoded_tree_representation__.append(self.__current_element__)
            self.__current_element__ = ''

    def get_representation(self):
        if len(self.__current_element__) != 0:
            self.__current_element__ += '0' * (8 - len(self.__current_element__))
            self.__encoded_tree_representation__.append(self.__current_element__)

        return ''.join(map(lambda bits: chr(int(bits, base=2)), self.__encoded_tree_representation__))


class BytesReader(object):
    def __init__(self, bytes_to_read):
        self.__bytes__ = bytes_to_read
        self.__next_byte__ = 0

    def get_numbers(self, number_of_numbers):
        number_of_bytes = 4 * number_of_numbers
        self.__next_byte__ += number_of_bytes
        return self.__bytes__[self.__next_byte__ - number_of_bytes: self.__next_byte__]

    def get_a_rest(self):
        return self.__bytes__[self.__next_byte__:]


class BitsReader(object):
    def __init__(self, bits_list):
        self.__bits__ = bits_list
        self.__next_bit__ = 0

    def next_bit(self):
        self.__next_bit__ += 1
        return self.__bits__[self.__next_bit__ - 1]

    def next_bits(self, number_of_bits):
        if number_of_bits == 1:
            return self.next_bit()
        else:
            return [self.next_bit() for _ in range(number_of_bits)]


class FileRepresentation(object):
    def __init__(self):
        self.__number_of_vertices__ = None
        self.__vertices__ = None
        self.__size_of_encoded_triangles__ = None
        self.__encoded_triangles__ = None
        self.__encoded_tree_representation__ = None

    def with_vertices(self, vertices):
        print 'Creating vertices representation... ',
        self.__number_of_vertices__ = struct.pack('I', len(vertices))

        coordinates_list = []

        for vertex in vertices:
            coordinates_list.append(vertex[0])
            coordinates_list.append(vertex[1])
            coordinates_list.append(vertex[2])

        self.__vertices__ = struct.pack(str(len(coordinates_list)) + 'f', *coordinates_list)

        print 'Done'
        return self

    def with_encoded_triangles(self, encoded_triangles):
        print 'Creating encoded triangles representation... ',
        self.__size_of_encoded_triangles__ = struct.pack('I', len(encoded_triangles))
        self.__encoded_triangles__ = struct.pack(str(len(encoded_triangles)) + 'I', *encoded_triangles)

        print 'Done'
        return self

    def with_encoded_tree(self, encoded_huffman_tree, encoded_tree):
        print 'Creating encoded tree representation... ',

        encoded_tree_representation_builder = EncodedTreeRepresentationBuilder()
        encoded_tree_representation_builder.add_bits(tuple(encoded_huffman_tree))

        for code in encoded_tree:
            encoded_tree_representation_builder.add_bits(code)

        self.__encoded_tree_representation__ = encoded_tree_representation_builder.get_representation()

        print 'Done'
        return self

    def write_to(self, file_directory, file_name):
        print 'Writing data to .ak file... ',
        file_path = file_directory + file_name + '.ak'

        with open(file_path, 'w') as result_file:
            result_file.write(self.__number_of_vertices__)
            result_file.write(self.__vertices__)
            result_file.write(self.__size_of_encoded_triangles__)
            result_file.write(self.__encoded_triangles__)
            result_file.write(self.__encoded_tree_representation__)

        print 'Done'

    def read_from(self, file_path):
        print 'Reading data from .ak file... ',
        with open(file_path, 'r') as compressed_file:
            bytes_reader = BytesReader(compressed_file.read())

        self.__number_of_vertices__ = bytes_reader.get_numbers(1)
        self.__vertices__ = bytes_reader.get_numbers(3 * struct.unpack('I', self.__number_of_vertices__)[0])
        self.__size_of_encoded_triangles__ = bytes_reader.get_numbers(1)
        self.__encoded_triangles__ = bytes_reader.get_numbers(struct.unpack('I', self.__size_of_encoded_triangles__)[0])
        self.__encoded_tree_representation__ = bytes_reader.get_a_rest()

        print 'Done'

    def get_vertices(self):
        print 'Retrieving vertices... ',
        vertices = []

        for i in range(0, len(self.__vertices__), 12):
            vertices.append(struct.unpack('3f', self.__vertices__[i: i + 12]))

        print 'Done'
        return vertices

    def get_encoded_triangles(self):
        print 'Retrieving encoded triangles... ',
        encoded_triangles = []

        for i in range(0, len(self.__encoded_triangles__), 4):
            encoded_triangles.append(struct.unpack('I', self.__encoded_triangles__[i: i + 4])[0])

        print 'Done'
        return encoded_triangles

    def get_encoded_tree(self):
        print 'Retrieving encoded tree... ',
        binary_strings_list = map(lambda char: bin(ord(char))[2:], list(self.__encoded_tree_representation__))
        binary_strings_list = map(lambda binary_string: '0' * (8 - len(binary_string)) + binary_string,
                                  binary_strings_list)
        bits_list = map(lambda string_bit: int(string_bit), [bit for sublist in binary_strings_list for bit in sublist])

        bits_reader = BitsReader(bits_list)

        encoded_huffman_tree = bits_reader.next_bits(15)

        possible_codes = triangle_tree_coding.possible_codes(encoded_huffman_tree)

        encoded_tree = []

        for _ in range(struct.unpack('I', self.__size_of_encoded_triangles__)[0] - 2):
            code = bits_reader.next_bit()

            if code in possible_codes:
                encoded_tree.append(code)
            else:
                code = (code, bits_reader.next_bit())

                if code in possible_codes:
                    encoded_tree.append(code)
                else:
                    encoded_tree.append(tuple(list(code) + [bits_reader.next_bit()]))

        print 'Done'
        return encoded_huffman_tree, encoded_tree


def file_representation():
    return FileRepresentation()
