# __author__ = 'Pawel Polit'

import numpy as np
from plyfile import PlyData, PlyElement

import compressed_file
import triangle_tree_coding
import triangle_tree


def start_decompression(file_path, text=False):
    file_representation = compressed_file.file_representation()
    file_representation.read_from(file_path)

    vertices = file_representation.get_vertices()
    encoded_triangles = file_representation.get_encoded_triangles()
    encoded_huffman_tree, encoded_tree = file_representation.get_encoded_tree()

    print 'Decoding triangle tree... ',
    triangle_tree_root = triangle_tree_coding.decode(encoded_triangles, encoded_tree, encoded_huffman_tree)
    print 'Done'

    print 'Retrieving triangles... ',
    triangles = triangle_tree.retrieve_triangles(triangle_tree_root)
    print 'Done'

    print 'Writing data to .ply file... ',
    triangles = map(lambda triangle: (triangle,), triangles)

    vertices = np.array(vertices, dtype=[('x', 'f4'), ('y', 'f4'), ('z', 'f4')])
    triangles = np.array(triangles, dtype=[('vertex_indices', 'i4', (3,))])

    vertices = PlyElement.describe(vertices, 'vertex')
    triangles = PlyElement.describe(triangles, 'face')

    file_name = file_path[file_path.rfind('/') + 1: -3]

    PlyData([vertices, triangles], text=text).write(file_name + '.ply')
    print 'Done'


start_decompression('compressed_file/triangle_mesh.ak', text=True)
