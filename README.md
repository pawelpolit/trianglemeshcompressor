# README #

Description of .ak file format:
1. number of vertices (int 4B)
2. vertices (list[float 4B], 3 floats represents 1 vertex coordinates)
3. size of encoded triangles (int 4B)
4. encoded triangles (list[int 4B], first and second ints are respectively left and right vertex of root in triangle tree, others are center vertices of triangles written in preorder)
5. encoded Huffman tree (always 15 bits = 4 leafs * 3 bits + 3 nodes * 1 bit), 1 -> node with children, 0 -> leaf, after 0 are 2 bits representing coded value, tree written in preorder)
6. encoded tree ( (0,0) -> without children, (0,1) -> only right child, (1,0) -> only left child, (1,1) -> two children, encoded with Huffman tree, written using bits in preorder, at end of last byte may be some unnecessary bits, you need to get rid of them using possible codes in Huffman coding and that the number of triangles = size of encoded triangles - 2