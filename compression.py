# __author__ = 'Pawel Polit'

from plyfile import PlyData

import triangle_tree
import triangle_tree_coding
import compressed_file


def start_compression(file_path):
    print 'Reading data from .ply file... ',
    ply_data = PlyData.read(file_path)
    vertices = ply_data.elements[0].data
    triangles = map(lambda x: x[0], ply_data.elements[1].data)
    print 'Done'

    print 'Creating triangle tree...',
    triangle_tree_root = triangle_tree.build(triangles)
    print 'Done'

    print 'Encoding triangle tree... ',
    encoded_triangles, encoded_tree, encoded_huffman_tree = triangle_tree_coding.encode(triangle_tree_root)
    print 'Done'

    directory_to_save_compressed_file = 'compressed_file/'
    file_name = file_path[file_path.rfind('/') + 1: -4]

    compressed_file.file_representation() \
        .with_vertices(vertices) \
        .with_encoded_triangles(encoded_triangles) \
        .with_encoded_tree(encoded_huffman_tree, encoded_tree) \
        .write_to(directory_to_save_compressed_file, file_name)


start_compression('ply_file/model_bin.ply')
