# __author__ = 'Pawel Polit'

from Queue import Queue
import numpy as np


class TriangleTreeNode(object):
    def __init__(self, triangle_index, parent=None):
        self.triangle_index = triangle_index
        self.parent = parent
        self.__children__ = {}
        self.__vertices__ = {}

    def new_triangle_index(self, triangle_index):
        self.triangle_index = triangle_index

    def set_left_child(self, child):
        self.__children__[0] = child

    def set_right_child(self, child):
        self.__children__[1] = child

    def set_left_vertex(self, vertex):
        self.__vertices__[0] = vertex

    def set_right_vertex(self, vertex):
        self.__vertices__[1] = vertex

    def set_center_vertex(self, vertex):
        self.__vertices__[2] = vertex

    def has_left_child(self):
        return 0 in self.__children__

    def has_right_child(self):
        return 1 in self.__children__

    def left_child(self):
        return self.__children__[0]

    def right_child(self):
        return self.__children__[1]

    def left_vertex(self):
        return self.__vertices__[0]

    def right_vertex(self):
        return self.__vertices__[1]

    def center_vertex(self):
        return self.__vertices__[2]

    def left_edge(self):
        return [self.left_vertex(), self.center_vertex()]

    def right_edge(self):
        return [self.center_vertex(), self.right_vertex()]


def edges(triangle):
    return [tuple(sorted(triangle[:2])), tuple(sorted(triangle[1:])), tuple(sorted(triangle[::2]))]


def neighbours(triangle_index, triangles, edge_to_triangles):
    return map(lambda edge: filter(lambda triangle: triangle != triangle_index, edge_to_triangles[edge])[0],
               filter(lambda edge: len(edge_to_triangles[edge]) == 2, edges(triangles[triangle_index])))


def prepare_edge_to_triangles(triangles):
    edge_to_triangles = {}

    for triangle_index in range(len(triangles)):
        triangle_edges = edges(triangles[triangle_index])

        for edge in triangle_edges:
            if edge in edge_to_triangles:
                edge_to_triangles[edge].append(triangle_index)
            else:
                edge_to_triangles[edge] = [triangle_index]

    return edge_to_triangles


def find_root_index(triangles, edge_to_triangles):
    for i in range(len(triangles)):
        if neighbours(i, triangles, edge_to_triangles) > 1:
            return i


def build(triangles):
    edge_to_triangles = prepare_edge_to_triangles(triangles)

    visited = [False] * len(triangles)
    queue = Queue()

    root_index = find_root_index(triangles, edge_to_triangles)

    root = TriangleTreeNode(root_index)
    visited[root_index] = True

    left_root_child, right_root_child = neighbours(root_index, triangles, edge_to_triangles)[:2]
    root.set_left_child(TriangleTreeNode(left_root_child, root))
    root.set_right_child(TriangleTreeNode(right_root_child, root))

    root_centre_vertex = reduce(np.intersect1d,
                                (triangles[root_index], triangles[left_root_child], triangles[right_root_child]))[0]

    root_left_vertex = filter(lambda v: v != root_centre_vertex,
                              np.intersect1d(triangles[root_index], triangles[left_root_child]))[0]

    root_right_vertex = filter(lambda v: v != root_centre_vertex and v != root_left_vertex, triangles[root_index])[0]

    root.set_left_vertex(root_left_vertex)
    root.set_right_vertex(root_right_vertex)
    root.set_center_vertex(root_centre_vertex)

    queue.put(root.left_child())
    queue.put(root.right_child())

    visited[left_root_child] = True
    visited[right_root_child] = True

    while not queue.empty():
        current_node = queue.get()

        current_node_children = filter(lambda neighbour: not visited[neighbour],
                                       neighbours(current_node.triangle_index, triangles, edge_to_triangles))

        if current_node.parent.has_left_child() and current_node is current_node.parent.left_child():
            current_node.set_left_vertex(current_node.parent.left_vertex())
            current_node.set_right_vertex(current_node.parent.center_vertex())
        else:
            current_node.set_left_vertex(current_node.parent.center_vertex())
            current_node.set_right_vertex(current_node.parent.right_vertex())

        current_node.set_center_vertex(
                filter(lambda vertex: vertex != current_node.left_vertex() and vertex != current_node.right_vertex(),
                       triangles[current_node.triangle_index])[0])

        if len(current_node_children) > 0:
            if len(np.intersect1d(current_node.left_edge(), triangles[current_node_children[0]])) == 2:
                current_node.set_left_child(TriangleTreeNode(current_node_children[0], current_node))
            else:
                current_node.set_right_child(TriangleTreeNode(current_node_children[0], current_node))

            visited[current_node_children[0]] = True

            if len(current_node_children) == 2:
                if current_node.has_left_child():
                    current_node.set_right_child(TriangleTreeNode(current_node_children[1], current_node))
                else:
                    current_node.set_left_child(TriangleTreeNode(current_node_children[1], current_node))
                visited[current_node_children[1]] = True

            if current_node.has_left_child():
                queue.put(current_node.left_child())

            if current_node.has_right_child():
                queue.put(current_node.right_child())

    return root


def retrieve_triangles(root):
    triangles = []

    stack = [root]

    while len(stack) > 0:
        current_node = stack.pop()

        while current_node.has_left_child():
            triangles.append([current_node.left_vertex(), current_node.center_vertex(), current_node.right_vertex()])

            if current_node.has_right_child():
                stack.append(current_node.right_child())

            current_node = current_node.left_child()

        triangles.append([current_node.left_vertex(), current_node.center_vertex(), current_node.right_vertex()])

        if current_node.has_right_child():
            stack.append(current_node.right_child())

    return triangles
